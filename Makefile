# Makefile for Hipo Lang
PACKAGE = com.harishlangs.
SRC = com\harishlangs\hipo
NAME = Hipo

all: compile run

repl: compile
	@echo "--> Running Hipo REPL"
	@cd java && java $(PACKAGE)hipo.$(NAME)

run: compile
	@echo "--> Running Hipo"
	@cd java && java $(PACKAGE)hipo.$(NAME) $(ARGS)

compile:
	@echo "--> Building Hipo Lang"
	@javac java\$(SRC)\\*.java

genast:
	@echo "--> Generating AST Tree"
	@javac java\com\harishlangs\tool\\*.java
	@cd java && java $(PACKAGE)tool.GenerateAst $(SRC)