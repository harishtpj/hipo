package com.harishlangs.hipo;

import java.util.List;
import java.util.Map;


class HipoClass implements HipoCallable {
    final String name;
    final HipoClass superclass;
    private final Map<String, HipoFunction> methods;

    HipoClass(String name,  HipoClass superclass, Map<String, HipoFunction> methods) {
      this.superclass = superclass;
      this.name = name;
      this.methods = methods;
    }

    HipoFunction findMethod(String name) {
      if (methods.containsKey(name)) {
        return methods.get(name);
      }

      if (superclass != null) {
        return superclass.findMethod(name);
      }
  
      return null;
    }

    @Override
    public String toString() {
      return name;
    }

    @Override
    public Object call(Interpreter interpreter,
                     List<Object> arguments) {
        HipoInstance instance = new HipoInstance(this);

        HipoFunction initializer = findMethod("init");
        if (initializer != null) {
          initializer.bind(instance).call(interpreter, arguments);
        }

        return instance;
    }

    @Override
    public int arity() {
        HipoFunction initializer = findMethod("init");
        if (initializer == null) return 0;
        return initializer.arity();
    }
}
