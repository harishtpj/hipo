package com.harishlangs.hipo;

import java.util.List;

interface HipoCallable {
    int arity();
    Object call(Interpreter interpreter, List<Object> arguments);
}
